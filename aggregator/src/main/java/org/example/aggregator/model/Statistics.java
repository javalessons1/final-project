package org.example.aggregator.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Data
public class Statistics {
    private String id;
    private String cardNumber;
    private String terminalId;
    private int amount;
    private String status;
    private String name;
    private String coordinates;
}
