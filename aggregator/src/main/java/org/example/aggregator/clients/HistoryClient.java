package org.example.aggregator.clients;

import org.example.aggregator.dto.PaymentDto;
import org.example.aggregator.dto.TerminalsIdsDto;
import org.example.aggregator.model.Payment;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

@FeignClient("history")
public interface HistoryClient {
    @GetMapping("/api/payments/{terminalId}")
    List<Payment> getStatisticsByStore(@PathVariable String terminalId);

    @PostMapping("payments")
    List<PaymentDto> getAllStatistics(@RequestBody TerminalsIdsDto terminalsIdsDto);
}
