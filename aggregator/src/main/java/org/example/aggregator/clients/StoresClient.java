package org.example.aggregator.clients;

import org.example.aggregator.dto.TerminalsDto;
import org.example.aggregator.model.Terminal;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;

@FeignClient("stores")
public interface StoresClient {
    @GetMapping("/terminals/{storeId}")
    TerminalsDto getTerminalById(@PathVariable final String storeId);
}
