package org.example.aggregator.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.example.aggregator.model.Status;
import org.hibernate.validator.constraints.CreditCardNumber;

import javax.validation.constraints.Min;
import java.util.UUID;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class PaymentDto {
    private String id = UUID.randomUUID().toString();
    private String terminalId;
    @CreditCardNumber
    private String cardNumber;
    @Min(1)
    private long amount;
    private long registered;
    private String status = Status.IN_PROGRESS;
}
