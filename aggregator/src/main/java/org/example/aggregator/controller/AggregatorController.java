package org.example.aggregator.controller;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.example.aggregator.dto.PaymentDto;
import org.example.aggregator.service.AggregatorService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequiredArgsConstructor
@Slf4j
public class AggregatorController {
    private final AggregatorService service;

    @GetMapping("/{storeId}/payment-statistics")
    public List<PaymentDto> getPaymentStatisticsByStoreId(@PathVariable String storeId) {
        return service.getAllPayments(storeId);
    }
}
