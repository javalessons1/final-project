package org.example.aggregator.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.example.aggregator.clients.HistoryClient;
import org.example.aggregator.clients.StoresClient;
import org.example.aggregator.dto.PaymentDto;
import org.example.aggregator.dto.TerminalsIdsDto;
import org.example.aggregator.model.Statistics;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
@Slf4j
@RequiredArgsConstructor
public class AggregatorService {
    private final HistoryClient historyClient;
    private final StoresClient storesClient;
    public  List<PaymentDto>  getAllPayments(final String terminalId) {
        log.info("request to stores by store_id: {}", terminalId);
        final List<String> terminalIds = storesClient.getTerminalById(terminalId).getTerminals();
        log.info("response from stores: {}", terminalIds);
        final TerminalsIdsDto terminalsIdsDto = new TerminalsIdsDto(terminalIds);
        log.info("request to history by terminal_ids: {}", terminalsIdsDto);
        final List<PaymentDto> payments = historyClient.getAllStatistics(terminalsIdsDto);
        log.info("response from history: {}", payments);
        return payments;
    }
}
