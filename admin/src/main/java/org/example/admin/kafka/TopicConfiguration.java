package org.example.admin.kafka;

import org.apache.kafka.clients.admin.NewTopic;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.config.TopicBuilder;

@Configuration(proxyBeanMethods = false)
public class TopicConfiguration {
  @Bean
  public NewTopic tasksTopic() {
    return TopicBuilder.name("tasks")
        .partitions(2)
        .replicas(3)
        .build();
  }

  @Bean
  public NewTopic paymentRegisteredTopic() {
    return TopicBuilder.name("registered")
            .partitions(2)
            .replicas(3)
            .build();
  }

  @Bean
  public NewTopic resultsTopic() {
    return TopicBuilder.name("results")
        .partitions(2)
        .replicas(3)
        .build();
  }
}
