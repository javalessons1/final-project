package org.example.history.entity;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.example.history.model.Status;

import javax.persistence.*;
import java.time.Instant;

@Entity(name = "payments")
@Getter
@Setter
@ToString
public class Payment {
    @Id
    private String id;
    @Column(name = "terminal_id")
    private String terminalId;
    @Column(name = "card_number")
    private String cardNumber;
    private long amount;
    private Instant registered;
    @Enumerated(EnumType.STRING)
    private String status;
}
