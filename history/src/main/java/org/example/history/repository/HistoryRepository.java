package org.example.history.repository;

import org.example.history.entity.Payment;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface HistoryRepository extends JpaRepository<Payment, String> {
  List<Payment> getPaymentsByTerminalId(final String terminalId);
  List<Payment> getPaymentsByTerminalIdIsIn(final List<String> terminalIds);
}
