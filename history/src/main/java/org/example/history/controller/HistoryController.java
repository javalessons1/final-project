package org.example.history.controller;

import lombok.RequiredArgsConstructor;
import org.example.history.dto.PaymentDto;
import org.example.history.dto.TerminalsIdsDto;
import org.example.history.service.HistoryService;
import org.springframework.web.bind.annotation.*;

import java.util.List;
@RestController
@RequiredArgsConstructor
public class HistoryController {
    private final HistoryService service;

    @GetMapping("{terminalId}/payments")
    public List<PaymentDto> getPaymentsByTerminalId(@PathVariable String terminalId) {
        return service.getPaymentsByTerminalId(terminalId);
    }

    @PostMapping("payments")
    public List<PaymentDto> getPaymentsByTerminalIds(@RequestBody TerminalsIdsDto terminalsIdsDto) {
        return service.getPaymentsByTerminalIds(terminalsIdsDto);
    }
}
