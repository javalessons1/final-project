package org.example.history.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.util.UUID;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class PaymentDto {
    private String id = UUID.randomUUID().toString();
    private String terminalId;
    private String cardNumber;
    private long amount;
    private long registered;
    private String status;
}
