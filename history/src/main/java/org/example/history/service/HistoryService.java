package org.example.history.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.example.history.dto.PaymentDto;
import org.example.history.dto.TerminalsIdsDto;
import org.example.history.entity.Payment;
import org.example.history.mapper.HistoryMapper;
import org.example.history.model.PaymentMessage;
import org.example.history.repository.HistoryRepository;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.support.Acknowledgment;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;


@Service
@RequiredArgsConstructor
@Slf4j
public class HistoryService {
    private final HistoryRepository repository;
    private final HistoryMapper mapper;

    @KafkaListener(topics = "${topic.after-processing}")
    private void save(final ConsumerRecord<String, PaymentMessage> message, final Acknowledgment acknowledgment) {
        log.info("received: {}", message);
        final PaymentDto received = message.value().getPayment();
        Payment saved = repository.save(mapper.fromPaymentDto(received));
        log.info("payment save: {}", saved);
        acknowledgment.acknowledge();
    }

    public List<PaymentDto> getPaymentsByTerminalId(final String terminalId) {
        return repository.getPaymentsByTerminalId(terminalId)
                .stream()
                .map(mapper::toPaymentDto)
                .collect(Collectors.toList());
    }

    public List<PaymentDto> getPaymentsByTerminalIds(final TerminalsIdsDto terminalsIdsDto) {
        return repository.getPaymentsByTerminalIdIsIn(terminalsIdsDto.getTerminalsIds())
                .stream()
                .map(mapper::toPaymentDto)
                .collect(Collectors.toList());
    }
}
