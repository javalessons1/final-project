package org.example.history.mapper;

import org.example.history.dto.PaymentDto;
import org.example.history.entity.Payment;
import org.mapstruct.Mapper;
import org.springframework.stereotype.Service;

import java.time.Instant;

@Mapper
@Service
public interface HistoryMapper {

    Payment fromPaymentDto(final PaymentDto paymentDto);

    PaymentDto toPaymentDto(final Payment payment);

    default Instant fromEpochMilli(final long milli) {
        return Instant.ofEpochMilli(milli);
    }

    default long fromInstant(final Instant instant) {
        return instant.toEpochMilli();
    }
}
