package org.example.producer.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.validation.annotation.Validated;

import javax.validation.Valid;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Data
@Validated
public class Payment {
  private String id;
  @Valid
  private String cardNumber;
  private String terminalId;
  @Valid
  private int amount;
  private String status;
}
