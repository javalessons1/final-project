package org.example.producer.controller;

import lombok.RequiredArgsConstructor;
import org.example.producer.model.Payment;
import org.example.producer.service.PaymentService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/payment")
@RequiredArgsConstructor
public class PaymentProducerController {
  private final PaymentService service;

  @PostMapping
  public Payment create(@Valid @RequestBody final Payment payment) {
    return service.sentPayment(payment);
  }
}
