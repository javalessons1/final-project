package org.example.producer.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.zookeeper.server.quorum.auth.QuorumAuth;
import org.example.producer.model.Payment;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import javax.validation.Valid;
import java.util.UUID;

@Service
@Slf4j
@RequiredArgsConstructor

public class PaymentService {
  private final KafkaTemplate<String, Payment> template;

  public Payment sentPayment(final Payment request) {
    final Payment response = Payment.builder()
        .id(UUID.randomUUID().toString())
        .cardNumber(request.getCardNumber())
        .terminalId(request.getTerminalId())
        .amount(request.getAmount())
        .build();
    sendToKafka(response);
    return response;
  }

  private void sendToKafka(final Payment request) {
      template.send("tasks", request).addCallback(
              result -> {
                  request.setStatus(String.valueOf(QuorumAuth.Status.IN_PROGRESS));
                  log.debug("sent to kafka: {}", request);
              },
              e -> {
                  request.setStatus(String.valueOf(QuorumAuth.Status.ERROR));
                  log.error("can't send to kafka: {}", request, e);
              }
      );
  }
}
