package org.example.consumer.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.example.consumer.model.Payment;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.Acknowledgment;
import org.springframework.stereotype.Service;
import org.example.consumer.model.Status;

@Service
@Slf4j
@RequiredArgsConstructor
public class ConsumerService {
  private final KafkaTemplate<String, Payment> template;

  @KafkaListener(topics = "tasks")
  public void listen(final ConsumerRecord<String, Payment> record, final Acknowledgment acknowledgment) {
    log.info("event received: {}", record);
    Payment paymentRegistered = record.value();
    paymentRegistered.setStatus(Status.IN_PROGRESS);
    acknowledgment.acknowledge();
    sendToKafka(paymentRegistered);
  }

  private void sendToKafka(final Payment request) {
    template.send("registered", request).addCallback(
            result -> {
              log.debug("sent to kafka: {}", request);
            },
            e -> {
              log.error("can't send to kafka: {}", request, e);
            }
    );
  }

}
