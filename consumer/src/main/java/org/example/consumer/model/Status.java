package org.example.consumer.model;

import lombok.Data;

@Data
public class Status {
    public static final String IN_PROGRESS = "IN PROGRESS";
    public static final String SUCCESS = "SUCCESS";
    public static final String FAIL = "FAIL";
}
