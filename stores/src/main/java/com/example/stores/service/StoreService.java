package com.example.stores.service;

import com.example.stores.dto.TerminalsDto;
import com.example.stores.entity.Store;
import com.example.stores.entity.Terminal;
import com.example.stores.repository.StoreRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@Slf4j
@RequiredArgsConstructor
public class StoreService {
  private final StoreRepository repository;

  public TerminalsDto getTerminalsByStoreId(final String storeId) {
    Store store = repository.getReferenceById(storeId);
    List<String> terminals = store.getTerminals().stream()
            .map(Terminal::getId)
            .collect(Collectors.toList());
    return new TerminalsDto(terminals);
  }
}
