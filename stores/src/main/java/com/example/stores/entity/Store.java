package com.example.stores.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import java.util.ArrayList;
import java.util.List;

@Entity(name = "stores")
@Getter
@Setter
public class Store {
    @Id
    private String id;
    private String name;
    @Column(name = "longitude_int")
    private int longitudeInt;
    @Column(name = "longitude_frac")
    private int longitudeFrac;
    @Column(name = "latitude_int")
    private int latitudeInt;
    @Column(name = "latitude_frac")
    private int latitudeFrac;
    @OneToMany(mappedBy = "store")
    private List<Terminal> terminals = new ArrayList<>();

}
