package com.example.stores.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity(name = "terminals")
@Getter
@Setter
public class Terminal {
    @Id
    private String id;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "store_id")
    private Store store;

}
