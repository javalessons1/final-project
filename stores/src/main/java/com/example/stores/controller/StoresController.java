package com.example.stores.controller;

import com.example.stores.dto.TerminalsDto;
import com.example.stores.service.StoreService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/stores")
@RequiredArgsConstructor
public class StoresController {
    private final StoreService service;

    @GetMapping("{storeId}/terminals")
    public TerminalsDto getTerminalsByStoreId(@PathVariable String storeId) {
        return service.getTerminalsByStoreId(storeId);
    }
}
