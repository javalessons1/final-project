package com.example.stores.repository;

import com.example.stores.entity.Store;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface StoreRepository extends JpaRepository<Store, String> {
    Store getReferenceById(String storeId);
}
